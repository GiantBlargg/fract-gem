#!/bin/sh
cd dist
git init
git config user.email "Giantblargg@bitbucket.org"
git config user.name "Giantblargg build bot"
git add .
git commit -m "Updated at: $(date)"
git remote add origin git@github.com:GiantBlargg/fract-gem.git
git push -fu origin master
