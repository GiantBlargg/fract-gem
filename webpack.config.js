var path = require("path");

module.exports = {
	context: path.resolve(__dirname, "src"),
	entry: "./index.ts",
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "dist")
	},
	resolve: { extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"] },
	devtool: "inline-source-map",
	module: {
		rules: [
			{ test: /\.glsl$/, use: "raw-loader" },
			{ test: /\.tsx?$/, use: "ts-loader" }
		]
	},
	devServer: {
		host: "0.0.0.0",
		contentBase: path.join(__dirname, "dist"),
		compress: true,
		disableHostCheck: true
	}
};
