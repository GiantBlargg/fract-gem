#version 300 es
precision highp float;

in vec2 pos;

uniform float vinRadi;

uniform float rippleSize;
uniform float rippleMaxStrength;

uniform vec3 baseColour;
uniform vec3 rippleColour;


struct ripple{
	float age;
	float strength;
	vec2 pos;
};

uniform struct{
	ripple ripples[128];
	int num;
}rip;

out vec4 result;

void main(void) {
	vec3 colour = baseColour;

	float sizeMod = rippleSize / rippleMaxStrength;
	for (int i = 0; i < rip.num; i++){
		ripple r = rip.ripples[i];
		float size = r.age * sizeMod-0.03;
		float strength = ((r.strength - r.age) / rippleMaxStrength);
		float dropoff = max(-(abs(size - distance(r.pos,pos))/0.03)+1.0,0.0);
		if (dropoff > 0.0)
			colour += rippleColour * pow(strength, 2.0) * dropoff;
	}


	float value = -(length(pos) / vinRadi) + 1.0;
	result = vec4(colour * value, 1.0);
}
