#version 300 es
precision highp float;

in vec2 pos;
in vec2 coord;

uniform sampler2D tex;

uniform float squaresTime;
uniform float spiralTime;

out vec4 result;

float spiralAngle(float radi){
	return 1.0*log(radi*1.0);
}

float angle(float angle1, float angle2) {
	float a = radians(10.0);
	float angleDiff = angle1 - angle2;
	angleDiff = mod(angleDiff+a,a*2.0)-a;
	return angleDiff;
}

bool checkSpiral(float r, float a) {
	vec2 spiral = vec2(0.0);
	spiral.x = spiralAngle(r);
	spiral.y = spiralAngle(r+0.05);
	return angle(spiral.x,a)>0.0 && angle(a,spiral.y)>0.0;

}

vec3 pattern(void){
	float value = 0.0;

	vec2 p = pos;
	p = p * 5.0 + sin(p.y + squaresTime) * 1.5;
	p.y = p.y + p.x;
	p = mod(p, 1.0);
	if(p.x < 0.1 || p.y < 0.1)
		value += 0.5;

	p = pos + vec2(sin(spiralTime),cos(spiralTime));
	float pi = radians(180.0);
	float r = length(p);
	float a = atan(p.y,p.x)-spiralTime; // to polar
	if(checkSpiral(r,a)||checkSpiral(r,-a))
		value += 0.5;

	//return -vec3(value, value, value)+1.0;
	return vec3(value, value, value);
}

void main(void) {
	result = texture(tex, coord);
	if (result.a<1.0)
		result = mix(result.rgba,vec4(pattern(),1.0), 1.0 - result.a);
	//result = vec4(pattern(),1.0);
}
