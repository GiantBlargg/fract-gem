#version 300 es
precision highp float;

in vec3 norm;
in vec3 pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform vec4 objectColour;
uniform vec3 lightColour;
uniform vec3 lightDir;
uniform float ambientStrength;
uniform float specStrength;

out vec4 result;

void main(void) {
	vec3 ambient = ambientStrength * lightColour * objectColour.rgb;

	//diffuse is calculated in world space.
	float diff = max(dot(normalize(norm), normalize(-lightDir)), 0.0);
	vec3 diffuse = diff * lightColour * objectColour.rgb;

	//specular is calculated in view space.
	vec3 reflectDir = reflect(normalize(mat3(transpose(inverse(view))) * lightDir), normalize(mat3(transpose(inverse(view))) * norm));
	float spec = pow(max(dot(vec3(0,0,1), reflectDir), 0.0), pow(2.0,6.0));
	vec3 specular = specStrength * spec * lightColour;

	vec3 colour = ambient + diffuse + specular;

	result = vec4(colour, objectColour.a);
}
