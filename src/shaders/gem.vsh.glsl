#version 300 es

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 norm;
out vec3 pos;

void main(void) {
	pos = vec3(model * vec4(position, 1.0));
	norm = mat3(transpose(inverse(model))) * normal;
	gl_Position = proj * view * model * vec4(position, 1.0);
}
