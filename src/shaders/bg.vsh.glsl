#version 300 es
precision highp float;

layout(location = 0) in vec2 position;

uniform float aspect;
uniform float desiredAspect;

out vec2 pos;

void main(void){
	vec2 size = vec2(1.0,1.0);

	if (desiredAspect < 1.0){
		size.x = size.y * desiredAspect;
	} else {
		size.y = size.x / desiredAspect;
	}

	if(aspect > desiredAspect) {
		size.x = size.y * aspect;
	} else {
		size.y = size.x / aspect;
	}
	pos = vec2(size.x*position.x,size.y*position.y);
	gl_Position = vec4(position.xy,0.9,1.0);
}
