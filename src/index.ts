
import Gem from "./scene/gem";
import BG from "./scene/bg";
import "fullscreen-api-polyfill";
import FrameBuffer from "./framebuffer";

let gl: WebGL2RenderingContext, aspect: number, gem: Gem, bg: BG, fb: FrameBuffer;

document.body.onload = start;

function start() {
	let canvas = <HTMLCanvasElement>document.getElementById("glCanvas");

	canvas.onclick = function () {
		if (document.fullscreenElement) {
			document.exitFullscreen();
		} else {
			canvas.requestFullscreen();
		}
	}

	gl = <WebGL2RenderingContext>canvas.getContext("webgl2");

	if (!gl) {
		throw "Could not create WebGL context";
	}

	init();
	window.requestAnimationFrame(draw);
}

function init() {
	gem = new Gem(gl);
	bg = new BG(gl);
	fb = new FrameBuffer(gl);

	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	//gl.cullFace(gl.FRONT);
}

function draw(time: number) {
	//Resize if need-be
	let width = gl.canvas.clientWidth * (devicePixelRatio || 1);
	let height = gl.canvas.clientHeight * (devicePixelRatio || 1);
	if (gl.canvas.width != width || gl.canvas.height != height) {

		gl.canvas.width = width;
		gl.canvas.height = height;

		aspect = width / height;

		fb.generate(aspect);
	}

	fb.bind();
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gem.render(fb.aspect(), time);
	bg.render(fb.aspect(), time);
	fb.unbind();

	gl.viewport(0, 0, width, height);

	fb.render(aspect, time);

	window.requestAnimationFrame(draw);
}
