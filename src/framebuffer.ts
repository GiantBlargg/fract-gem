import config from "./config";
import Program from "./shader";

export default class FrameBuffer {
	private readonly shaderProgram: Program;
	private readonly VAO: WebGLVertexArrayObject;

	private ID: WebGLFramebuffer;
	private texture: WebGLTexture;
	private depth: WebGLRenderbuffer;

	private size: { width: number; height: number; offWidth: number; offHeight: number; };
	constructor(readonly gl: WebGL2RenderingContext) {
		this.shaderProgram = new Program(gl, "fb.vsh", "fb.fsh");

		//Buffers
		this.VAO = gl.createVertexArray();
		let VBO = gl.createBuffer();

		gl.bindVertexArray(this.VAO);

		gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, 1, 1, -1, 1]), gl.STATIC_DRAW);

		gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 8, 0);
		gl.enableVertexAttribArray(0);

		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindVertexArray(null);
	}

	render(aspect: number, time: number) {
		this.shaderProgram.use();

		this.shaderProgram.uniform(1, "squaresTime", time / config.fb.squaresSpeed);
		this.shaderProgram.uniform(1, "spiralTime", time / config.fb.spiralSpeed);
		this.shaderProgram.uniform(1, "aspect", aspect);
		this.shaderProgram.uniform(1, "desiredAspect", config.transform.proj.width / config.transform.proj.height);
		this.shaderProgram.uniform(2, "sizeMod", [1 - this.size.offWidth, 1 - this.size.offHeight]);

		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
		this.gl.bindVertexArray(this.VAO);
		this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, 4);
	}

	bind() {
		this.gl.viewport(0, 0, this.size.width, this.size.height)
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.ID);
	}
	unbind() {
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
	}

	aspect() {
		return this.size.width / this.size.height;
	}

	generate(aspect: number) {
		this.ID = this.gl.createFramebuffer();
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.ID);

		this.size = findFBsize(aspect);

		this.texture = this.gl.createTexture();
		this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);

		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.size.width, this.size.height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);

		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);

		this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, this.texture, 0);

		this.depth = this.gl.createRenderbuffer();
		this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, this.depth);
		this.gl.renderbufferStorage(this.gl.RENDERBUFFER, this.gl.DEPTH_COMPONENT16, this.size.width, this.size.height);
		this.gl.framebufferRenderbuffer(this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT, this.gl.RENDERBUFFER, this.depth);

		if (this.gl.checkFramebufferStatus(this.gl.FRAMEBUFFER) != this.gl.FRAMEBUFFER_COMPLETE) {
			throw "Framebuffer failed";
		}

		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
	}
}

function findFBsize(aspect: number) {
	let width = config.fb.size.width;
	let height = config.fb.size.height;
	let desiredAspect = config.transform.proj.width / config.transform.proj.height;

	if (desiredAspect > 1) {
		width = height * desiredAspect;
	} else {
		height = width / desiredAspect;
	}

	if (aspect > desiredAspect) {
		width = height * aspect;
	} else {
		height = width / aspect;
	}

	let size = {
		width: Math.ceil(width / 2) * 2,
		height: Math.ceil(height / 2) * 2,
		offWidth: 0,
		offHeight: 0
	};
	size.offWidth = (size.width - width) / size.width;
	size.offHeight = (size.height - height) / size.height;
	return size;
}
