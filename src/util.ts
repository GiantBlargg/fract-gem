import { mat4 } from "gl-matrix";

export function makeProj(aspect: number, width: number, height: number, depth: number) {
	let desiredAspect = width / height;
	if (aspect > desiredAspect) {
		width = height * aspect;
	} else {
		height = width / aspect;
	}
	return mat4.ortho(mat4.create(), -width / 2, width / 2, -height / 2, height / 2, 0, depth);
}

export function getRandom(min: number, max: number) {
	return Math.random() * (max - min) + min;
}
