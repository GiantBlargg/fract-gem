import Program from "../shader";
import { mat4 } from "gl-matrix";
import config from "../config";
import { makeProj } from "../util";

export default function applyTransform(prog: Program, aspect: number) {
	let view = mat4.lookAt(mat4.create(), config.transform.view.eye, config.transform.view.centre, config.transform.view.up);
	prog.uniformMatrix("view", view);

	let proj = makeProj(aspect, config.transform.proj.width, config.transform.proj.height, config.transform.proj.depth);
	prog.uniformMatrix("proj", proj);
}
