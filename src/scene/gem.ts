import { mat4 } from "gl-matrix";
import config from "../config";
import Program from "../shader";
import applyTransform from "./transform";

export default class Gem {
	private readonly shaderProgram: Program;
	private readonly VAO: WebGLVertexArrayObject;
	constructor(readonly gl: WebGL2RenderingContext) {

		//Shaders
		this.shaderProgram = new Program(gl, "gem.vsh", "gem.fsh");

		//Buffers
		this.VAO = gl.createVertexArray();
		let VBO = gl.createBuffer();

		gl.bindVertexArray(this.VAO);

		gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(config.gem.vertecies), gl.STATIC_DRAW);

		gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 24, 0);
		gl.enableVertexAttribArray(0);

		gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 24, 12);
		gl.enableVertexAttribArray(1);

		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindVertexArray(null);
	}

	render(aspect: number, time: number) {
		this.shaderProgram.use();

		applyTransform(this.shaderProgram, aspect);

		let model = mat4.create();
		mat4.rotateZ(model, model, time / config.gem.speed);
		this.shaderProgram.uniformMatrix("model", model);

		this.shaderProgram.uniform(4, "objectColour", config.gem.colour);
		this.shaderProgram.uniform(3, "lightColour", config.light.colour);
		this.shaderProgram.uniform(3, "lightDir", config.light.dir);
		this.shaderProgram.uniform(1, "ambientStrength", config.light.ambientStrength);
		this.shaderProgram.uniform(1, "specStrength", config.gem.specStrength);

		this.gl.bindVertexArray(this.VAO);
		this.gl.drawArrays(this.gl.TRIANGLES, 0, config.gem.vertecies.length / 6);
	}
}
