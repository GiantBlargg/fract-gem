import Program from "../shader";
import config from "../config";
import { getRandom } from "../util";

interface ripple { age: number; strength: number; x: number; y: number }

export default class BG {
	private readonly shaderProgram: Program;
	private readonly VAO: WebGLVertexArrayObject;
	private readonly ripples: ripple[] = [];
	constructor(readonly gl: WebGL2RenderingContext) {
		//Shaders
		this.shaderProgram = new Program(gl, "bg.vsh", "bg.fsh");

		//Buffers
		this.VAO = gl.createVertexArray();
		let VBO = gl.createBuffer();

		gl.bindVertexArray(this.VAO);

		gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, 1, 1, -1, 1]), gl.STATIC_DRAW);

		gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 8, 0);
		gl.enableVertexAttribArray(0);

		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindVertexArray(null);
	}

	render(aspect: number, time: number) {
		this.shaderProgram.use();

		//Gen ripple
		if (Math.random() < config.bg.ripple.chance) {
			let rip = <ripple>{};
			rip.age = time;
			rip.strength = getRandom(config.bg.ripple.strength.min, config.bg.ripple.strength.max);
			rip.x = getRandom(-2, 2);
			rip.y = getRandom(-2, 2);
			this.ripples.push(rip);
		}

		//Remove expired ripples
		for (let i = this.ripples.length - 1; i > -1; i--) {
			if ((time - this.ripples[i].age) > this.ripples[i].strength)
				this.ripples.splice(i, 1);
		}

		this.shaderProgram.uniformi(1, "rip.num", this.ripples.length);
		for (let i = 0; i < this.ripples.length; i++) {
			this.shaderProgram.uniform(1, "rip.ripples[" + i + "].age", time - this.ripples[i].age);
			this.shaderProgram.uniform(1, "rip.ripples[" + i + "].strength", this.ripples[i].strength);
			this.shaderProgram.uniform(2, "rip.ripples[" + i + "].pos", this.ripples[i].x, this.ripples[i].y);
		}

		this.shaderProgram.uniform(1, "aspect", aspect);
		this.shaderProgram.uniform(1, "desiredAspect", config.transform.proj.width / config.transform.proj.height);
		this.shaderProgram.uniform(1, "vinRadi", config.bg.vinRadi);
		this.shaderProgram.uniform(3, "baseColour", config.bg.baseColour);
		this.shaderProgram.uniform(3, "rippleColour", config.bg.ripple.colour);
		this.shaderProgram.uniform(1, "rippleSize", config.bg.ripple.size);
		this.shaderProgram.uniform(1, "rippleMaxStrength", config.bg.ripple.strength.max);

		this.gl.bindVertexArray(this.VAO);
		this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, 4);
	}
}
